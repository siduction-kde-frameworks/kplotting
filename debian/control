Source: kplotting
Section: libs
Priority: optional
Maintainer: Debian/Kubuntu Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
Uploaders: Maximiliano Curia <maxy@debian.org>
Build-Depends: cmake (>= 2.8.12),
               debhelper (>= 9),
               extra-cmake-modules (>= 5.11.0~),
               pkg-kde-tools (>= 0.15.15ubuntu1~),
               qtbase5-dev (>= 5.4),
               xauth,
               xvfb
Standards-Version: 3.9.6
XS-Testsuite: autopkgtest
Homepage: https://projects.kde.org/projects/frameworks/kplotting
Vcs-Browser: http://anonscm.debian.org/cgit/pkg-kde/frameworks/kplotting.git
Vcs-Git: git://anonscm.debian.org/pkg-kde/frameworks/kplotting.git

Package: libkf5plotting-dev
Section: libdevel
Architecture: any
Depends: libkf5plotting5 (= ${binary:Version}),
         qtbase5-dev (>= 5.4),
         ${misc:Depends}
Description: development files for kplotting
 KPlotWidget is a QWidget-derived class that provides a virtual base class
 for easy data-plotting. The idea behind KPlotWidget is that you only have
 to specify information in "data units"; i.e., the natural units of the
 data being plotted.  KPlotWidget automatically converts everything
 to screen pixel units.
 .
 Contains development files for kplotting.

Package: libkf5plotting5
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: KPlotting provides classes to do plotting.
 KPlotWidget is a QWidget-derived class that provides a virtual base class
 for easy data-plotting. The idea behind KPlotWidget is that you only have
 to specify information in "data units"; i.e., the natural units of the
 data being plotted.  KPlotWidget automatically converts everything
 to screen pixel units.

Package: libkf5plotting5-dbg
Section: debug
Priority: extra
Architecture: any
Multi-Arch: same
Depends: libkf5plotting5 (= ${binary:Version}), ${misc:Depends}
Description: debug symbols for kplotting
 KPlotWidget is a QWidget-derived class that provides a virtual base class
 for easy data-plotting. The idea behind KPlotWidget is that you only have
 to specify information in "data units"; i.e., the natural units of the
 data being plotted.  KPlotWidget automatically converts everything
 to screen pixel units.
 .
 Contains debug symbols for kplotting.
